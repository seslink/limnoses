
#   Function to restructure a data frame with agent information over time to a table comprising one line for each agent
def dfSim_to_dfAgent(df):
    df = df.sortlevel() # first along id's, then years
    del df[' ']
    
    # 1. Create new df with agent id's and first year data
    df_agents = df.swaplevel('id', 'year').loc[1, :] # one apparently can only select from the first index, that is why swapping became necessary
    del df_agents['oss']
    
    # 2. Identify year of oss update
    df['oss-diff'] = df['oss'].shift() - df['oss']
    df_end = df[df['oss-diff'] == -1] # this is the row for each agent when he upgraded the oss
    
    # look for the minimum year index, which is the year when the first houseowners upgraded
    yinformed = df_end.idxmin()['time-lag'][1] 
    # look for the time-lag that a houseowner had when he upgraded in the first year possible, and calculates the year when informed from that
    yearInformed = yinformed - df_end.swaplevel(1,0).ix[yinformed]['time-lag'].min()
    assert yearInformed >= 0, 'MinIndex: {}, year Informed: {}'.format(yinformed,yearInformed)
    
    # 3. Put first year and upgrade-year information together in one df
    df_end.index = df_end.index.droplevel('year')
    df_agents['comp-last'] = df_end.compliance
    df_agents['time-lag'] = df_end['time-lag'] # overwrite first years time-lag with upgrade-year time-lag
    
    return df_agents, yearInformed
  

# Function to extract the max. sewage upgrade efficiency and the half saturation constant
def calcEfficiencyfromDfSim(df, yearInformed):
    del df[' ']
    del df['bream']
    del df['pike']
    del df['nutrients']
    del df['vegetation']
    
    # selection of rows, one per year, where upgrade efficiency develops (> 0 and < 100)
    a = df[df['upgrade-efficiency'] > 0]
    b = a[a['upgrade-efficiency'] < 100]
    b['h-diff'] = b['upgraded households'] - b['upgraded households'].shift()
    c = b[b['h-diff'] != 0]
    maxEff = c['upgrade-efficiency'].max()
    
    # determine time until 50% of households upgraded
    df = c[c['upgraded households'] <= c['upgraded households'].max()/2] # works only if all(!) households upgrade eventually
    if len(df.index.tolist()) > 0:
        years_50_upgraded = max(df.index.tolist())[0] # gets the first index from the data frame before 50% are reached
    else:
        df = c[c['upgraded households'] > 0]
        years_50_upgraded = min(df.index.tolist())[0]
        
    yearsHalfUpgrade = years_50_upgraded - yearInformed
    
    return maxEff, yearsHalfUpgrade

# Function to select an annual time series part from the tick-based series
def slice_restoration_effects(df, nCrit, pikeCrit, evalRange):
    # select the first evalRange of annual entries starting with the yearInformed
    a = df_ticks[df_ticks['upgraded households'] > 0]
    annual_nutrients = a['nutrients'].groupby(level=0).first()[:evalRange]
    annual_pike = a['pike'].groupby(level=0).mean()[:evalRange]
    yearIx = a.groupby(level=0).first()[:evalRange].reset_index()['year']

    # deducting the threshold value
    nutrient_diff = annual_nutrients - nCrit
    pike_diff = annual_pike - pikeCrit
    
    return yearIx, nutrient_diff, pike_diff