About
=====

LimnoSES is a NetLogo based simulation model that represents a social-ecological system (SES) of shallow lake use and management. It is capable to exhibit ecological regime shifts between the turbid and the clear state which can be influenced by individual agents polluting the lake. The ecological model is a reimplementation from Scheffer 1989 using the NetLogo system dynamics extension and it is interacting with the agent-based social model. Details on the research background can be found in Martin and Schlüter 2015.

![Icon.png](https://bitbucket.org/repo/L45Bee/images/206971992-Icon.png)

Content
=======

 * The main NetLogo implementation: LimnoSES.nlogo 
 * The model documentation based on the ODD+D protocol (Müller et al. 2013): LimnoSES_ODD.docx.
 * Jupyter notebooks to examine simulation experiments.

Required installations
======================

To run a NetLogo simulation, one needs to [install NetLogo](http://ccl.northwestern.edu/netlogo/download.shtml) and Java. One starts NetLogo with `java -jar NetLogo.jar` and opens therein the particular model *.nlogo.
To run model analyses, it is recommended to install Jupyter with [Python](http://jupyter.readthedocs.org/en/latest/install.html) since all attached analyses are provided as Jupyter notebooks.
Make sure that your local repository has the subfolder simdata in it before running any simulations.

Things to try
=============

Besides single simulations that can be started from the NetLogo user interface, it is recommended to run experiments from the BehaviorSpace. This will generate simulation data from varied settings and with sufficient repetitions to reproduce analyses with the corresponding notebooks. So far tested experiments are:

 * Rtipping_demo (reproduces Figure 5 in Martin and Schlüter 2015)
 * [TurbidityResponse_demo](http://nbviewer.jupyter.org/urls/bitbucket.org/seslink/limnoses/raw/master/LimnoSES_TurbidityResponseDemo.ipynb) (reproduces Figure 6 in Martin and Schlüter 2015)

Ways to contribute
==================

 * Model analyses depends on suitable simulation experiments to test hypotheses. Do you wish to add particular experiments in the NetLogo BehaviorSpace? Suggestions are welcome!
 * Critical tests of the implementation allow a more robust development. Which tests should be added?
 * Aggregate evaluations and visualization of large sets of model simulations are important to improve the understanding of the modeled system. Which experiment evaluations should be added? (preferably in Jupyter notebooks)
 * The current model is quite limited in its social-ecological interactions, it is including monitoring and polluting of the lake. Since this model is focusing on those interactions, further ways to use or manage the lake are welcome extensions.

Contributors
============

 * Romina Martin romina.martin@su.se
	
References
==========

 * Martin, Romina, and Maja Schlüter. 2015. “Combining System Dynamics and Agent-Based Modeling to Analyze Social-Ecological Interactions—an Example from Modeling Restoration of a Shallow Lake.” Frontiers in Environmental Science 3 (October): 1–15. doi:10.3389/fenvs.2015.00066.
 * Müller, Birgit, Friedrich Bohn, Gunnar Dreßler, Jürgen Groeneveld, Christian Klassert, Romina Martin, Maja Schlüter, et al. 2013. “Describing Human Decisions in Agent-Based Models - ODD+D, an Extension of the ODD Protocol.” Environmental Modelling and Software 48: 37–48. doi:http://dx.doi.org/10.1016/j.envsoft.2013.06.003.
 * Scheffer, M. 1989. “Alternative Stable States in Eutrophic, Shallow Freshwater Systems: A Minimal Model.” Hydrobiological Bulletin 23 (1) (March): 73–83. doi:10.1007/BF02286429.