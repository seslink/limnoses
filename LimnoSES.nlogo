;;; LimnoSES
;;; @author Romina Martin
;;; @email romina.martin@su.se
;;; @version 1.1.0
;;; @date 2019-01-10

; social
breed [households household]
households-own [
  information ;1:notified about rules, lake state, 0: knows nothing
  compliance  ;1:yes, 0:no
  oss         ;1:modern, 0:old
  time-lag-for-implementation]

breed [municipalities municipality]
municipalities-own [
  money ; without consequences
  legislation
  information]
; money to spend on monitoring/rule enforcement, legislation power to enforce new rules, information on state of the lake

; ecological
breed [whitefishs whitefish] ;= cyprinids = bream
breed [pikes pike]

globals [
  coast-size
  city-size
  lake-size
  ;nutrients  ; moved to system dynamics
  transient-nutrients ; variable for linear function of nutrient in-/decrease
  affectors ; number of polluting agents
  bream-this-year ; stock size remembered for biomanipulation activity
  biomanipulate-this-year; true/false whether to catch bream
  ;tolerance-level-affectors ; [%] moved to user interface
  lake-vegetation
  coast-patches
  days-to-catch-bream ; biomanipulation days per year
  lake-patches
  veg-patches
  number-of-lake-patches
  msg-counter
  filename
  filename-houseowners ; annual report of agent states
  filename-once ; report of variables at one point during simulation
  filename-lake ; report annual values from lake variables (not aggregated, just one snapshot; as alternative to tick-based)
  path ; common part of filenames
  time-to-finish ; number of years before simulation is stopped
  years ; just counting the years while ticks is for daily steps
  year-when-informed ; remembers the first year when municipality was informed about critical lake status from monitoring
  year-when-pike-became-critical ; remember the first year pike is below expectation level
  year-when-nutrients-became-critical ; remember the first year nutrients are above 1expectation level
  year-when-desired-level-is-back ;
  year-when-desired-pike-is-back;
  upgraded-households-sum ; aggregates the number of upgraded oss each year
  year-of-full-upgrade; remeber the year when all households finished their update
  ;run-number ; does nothing, this is just a counter to generate unique filenames for a set of stochastic simulations without parameter change
  ]

extensions [profiler]

to setup
  ;;; sets up the simulation
  clear-all
  if profiling? [profiler:start]

  ;initialize some global variables
  set coast-size 40 ; size of non-lake area
  set city-size 15 ; size of area not populated by single households
  set lake-size max-pycor * (max-pxcor - coast-size)
  set lake-vegetation 50 ; percentage of cover by vegetation, is later updated in SD setup
  set transient-nutrients init-nutrients
  set time-to-finish 100 ; maximum number of simulation years. It is reduced by 'update-settings'
  if experiment != "none" [update-settings-once] ; from user choices of experiments
  set msg-counter 1 ; counter used to show up in all messages

  ; initialize patches
  set coast-patches patch 0 0 ; to initialize this patch-set
  ask patches [setup-landscape]
  set lake-patches patches with [pcolor = blue] ; patch-set
  set veg-patches patches with [pcolor = green] ; patch-set
  set number-of-lake-patches count lake-patches + count veg-patches

  ; initialize agents with symbol, color, place
  set bream-stock initial-number-bream
  set pike-stock initial-number-pike
  setup-agents
  set biomanipulate-this-year biomanipulate?
  ;set phytoplankton (count patches with [pcolor = green])

  system-dynamics-setup
  system-dynamics-do-plots
  setup-plots
  ;update-plot4
  ;reset-ticks ;was called in system-dynamics-setup

  create-files ; for monitoring tick-based, annual-based, and run-based data
  setup-tests ; checks for unintended input values
end

to setup-landscape ; deals with all the patches
  ;;; setup for lake patches - only for demonstration purposes, no effect back on the simulation
  ; coloring coast and lake
  ifelse pxcor < (min-pxcor + coast-size)
    [ set pcolor 52
      set coast-patches (patch-set self coast-patches)
     ] ; coast
    [; lake with % vegetation covered green
      ifelse random-float 1 > (lake-vegetation / 100)
      [set pcolor blue]
      [set pcolor green]
    ]
end

to setup-agents
  ;;; does the setup for all the agents in and at the lake

  ;putting people on the coast
  set-default-shape households "house stuga"
  create-households initial-number-households
  [
    set color 25
    set size 3
    setxy (3 + random (coast-size - 3)) (1 + random (max-pycor - city-size))
    set information 0
    set oss 0
    ifelse agents-uniform?
    [set compliance random-float 1]
    [if houseowner-types != "uniform"
      [set compliance willingness-to-upgrade]
    ]
    set time-lag-for-implementation "none"
  ]

  set-default-shape municipalities "house municipality"
  create-municipalities 1
  [
    set color 27
    set size 4.5
    setxy 2 max-pycor - city-size + 5
    set money 10
    set information 0
    set legislation 0
  ]

  set-default-shape whitefishs "fish"
  set-default-shape pikes "shark"
  update-bream
  update-pike

end

to setup-tests
    ; check on whether the interface input is reasonable
    if target-nutrients < 0.4 [print (word "Target nutrients set too low " target-nutrients ", transient-n: " transient-nutrients)]
end

;end of setup
;________________________________________________________________________________________________________________________________________

to go
  ;;; Simulation of daily steps with 1st house owners deciding on oss upgrad, 2nd nutrient based system dynamics, 3rd pontial municipality decisions and finally,
  ;;; if checked, saving data to tick-based file

;********************************
; 1. Householders decide on oss update
;********************************
  if ticks mod 365 = 0 ; just once a year householders may update their oss
  [
    set years years + 1
    ask households with [oss = 0 and information = 1]
    [
      ; update households sewage system
      ifelse random-float 1 < compliance
      [
        set oss 1
        set color 38
        if houseowner-types = "homoOwnersSocial" ; then tell your neighbors and that will increase their compliance
        [
          ask other households in-radius radius-of-neighbors with [oss = 0]
          [
            set color 45
            set compliance compliance * 1.5
            if compliance > 1 [set compliance 0.99]
          ]
        ]
      ]
      [ ; if houseowner does not updates oss, it's time-lag is counted up
        set time-lag-for-implementation time-lag-for-implementation + 1
      ]
    ]
    ; update pollution level: nutrient inflow by affectors
    set affectors (count households with [ oss =  0 ])

    ; checking the success of upgrade
    if (year-when-informed > 0) and (year-of-full-upgrade <= years)
    [
      set upgraded-households-sum upgraded-households-sum + count households with [oss = 1]
    ]

    if log-agents?    ; record household characteristics for each year
    [
      file-open filename-houseowners
      if years = 1 [file-print " "]

      ; record household characteristics for each year
      ask households
      [
        file-type years file-type ","
        file-type who file-type ","
        file-type compliance file-type ","
        file-type time-lag-for-implementation file-type ","
        file-type oss file-type ","
        file-print " "
      ]
      file-close
    ]
  ] ;once a year

;********************************
; 2. Nutrients affect system dynamics of the lake: nutrients -> vegetation, fish
;********************************
  if ticks mod 365 = 0 [
    update-settings ; for experiments that change settings over certain periods
    update-nutrients
    ; log annual values
    if log-lake-per-year?
    [
      file-open filename-lake
      file-print " "
      file-type years file-type ","
      file-type bream-stock file-type ","
      file-type pike-stock file-type ","
      ;file-type algae file-type ","
      file-type constant-or-dynamic-nutrients file-type ","
      file-type upgrade-efficiency file-type ","
      file-type count households with [oss = 1] file-type ","
      file-type sewage-water file-type ","
      file-close
    ]
  ]
  if time-to-finish < 1 [stop]
  system-dynamics-go ; is this causing a tick? It looks like part 1 of the go function is happening one year/tick earlier than part 3
  update-bream
  update-pike
  update-vegetation
  system-dynamics-do-plots
  if year-when-nutrients-became-critical = 0 and nutrients > critical-nutrient
  [set year-when-nutrients-became-critical years]

  if biomanipulate-this-year = true
  [set days-to-catch-bream days-to-catch-bream - 1]

;********************************
; 3. Municipality decides on sewage treatment rule
;********************************
  ; annual activities:
  if (ticks mod 365 = 0 and regulate?)
  [
    ; 1st: Checking whether update of rules is necessary
    ;when information from monitoring indicates need to act/rule, the legislation status is updated
    if (min [information] of municipalities = 1) and (min [legislation] of municipalities = 0)
    [
      ask municipalities
      [
        set legislation 1  ; turn information of pollution into a policy of sewage treatment
        set color 17
      ]
      set year-when-informed years - 1 ; remember this as t1 for later calculation of implementation efficiency
      if (messages?) [
        print (word "Msg "msg-counter": In year "years", the municipality publishes new rules on necessary oss upgrades.")
        set msg-counter msg-counter + 1
      ]
    ]

    ;2nd activity: monitoring (in this order to prevent immediate action based on new monitoring data)
    ask municipalities with [information = 0]
    [
      do-monitoring
      if (messages?)
      [
        print (word "Msg "msg-counter": Monitoring was done in year"years", tick"ticks" ")
        set msg-counter msg-counter + 1
      ]
    ]

    ;3rd activity: informing households, eventually enforcing
     if (min [legislation] of municipalities = 1) and (count households with [oss = 0]) > 0
    [
      if (count households with [oss = 1] = 0)
      [
        inform-households
      ]

      ; Enforcement after 5 years
      if houseowner-types = "homoOwnersEnforced"
      [
        ask households with [time-lag-for-implementation > 4]
        [
          set compliance compliance * 1.5   ; compliance increases by 50%
          if compliance > 1
            [set compliance 0.99]
          if messages?
          [
            print (word "Msg "msg-counter": The municipality has sent out oss inspectors (year "years")")
            set msg-counter msg-counter + 1
          ]
        ]
      ]
    ]
    ; remember the year when all households were upgraded
    if ((count households with [oss = 1]) = initial-number-households) and year-of-full-upgrade = 0
      [set year-of-full-upgrade years]

    ; remember the year when the desired state is restored
    if min [legislation] of municipalities = 1 and year-when-desired-level-is-back = 0; look only for this year after degradadion and regulation of system has started
    [
      if threshold-variable = "pike"
      [
        if pike-stock > pike-expectation [set year-when-desired-level-is-back years]
      ]
      if threshold-variable = "nutrients"
      [
        if nutrients < critical-nutrient [set year-when-desired-level-is-back years]
      ]
    ]
    if min [legislation] of municipalities = 1 and year-when-pike-became-critical > 0 and year-when-desired-pike-is-back = 0
    [
       if pike-stock > pike-expectation [set year-when-desired-pike-is-back years]
    ]
    if log-laststate? [ save-state ]

    ;4th activity - biomanipulation
    ; when does the municipality start/end biomanipulation?
    if biomanipulate? = true [
        set bream-this-year bream-stock
        set days-to-catch-bream days-per-year
        set biomanipulate-this-year true
    ]
  ] ; end of annual municipality activities

  if log-ticks?
  [
    ;*************************************
    ; report results and write into a file
    file-open filename
    file-print " "
    file-type ticks file-type ","
    file-type bream-stock file-type ","
    file-type pike-stock file-type ","
    ;file-type algae file-type ","
    file-type constant-or-dynamic-nutrients file-type ","
    file-type vegetation file-type ","
    file-type years file-type ","
    file-type upgrade-efficiency file-type ","
    file-type count households with [oss = 1] file-type ","
    file-close
  ]
  ;tick
  update-plots ; this is still necessary for the non-system-dynamics plot-pens


  ; profiling only for the first 20 years of a simulation
  if profiling?
  [
    if years = 20 and ticks = 7300
    [
      profiler:stop          ;; stop profiling
      print profiler:report  ;; view the results
      profiler:reset
    ]
  ]
end ;go

;________________________________________________________________________________________________________________________________________


to-report upgrade-efficiency
  ;;; idle - Attempt to visualize how fast house owners do the upgrade in relation to the time since when they have been informed.
  ;;; This data is collected per-year and per-tick.
  ;;; @report current-efficiency - Percentage of house owners who upgraded their OSS.
  ifelse year-when-informed > 0 and years > year-when-informed
  [
    ifelse year-of-full-upgrade > 0 and year-of-full-upgrade < years
    [; case for all house owners did upgrade instantaneously
      report 100
    ]
    [
      let t1 year-when-informed
      let t2 years
      if year-of-full-upgrade > 0   ; in this case the evaluated period is fixed
        [set t2 year-of-full-upgrade]
      let optimal-efficiency (t2 - t1) * initial-number-households
      let current-efficiency 0

      set current-efficiency upgraded-households-sum * 100 / optimal-efficiency

      report current-efficiency
    ]
  ]
  [
    report 0
  ]
end


to do-monitoring
  ;;; This is a municipality-actor function which evaluates the need for legislation based on the flag 'respond-direct?', the pike or nutrient variable
  ;;; to check threshold values for.

  let trigger random-float 1 ; option to include some stochasticty when option respond-direct? is false.
  if respond-direct? [set trigger 0]
  let threshold-monitored? "false"

  if threshold-variable = "pike"
  [
    set threshold-monitored? trigger < pike-loss-perception
  ]
  if threshold-variable = "nutrients"
  [
    set threshold-monitored? nutrients > critical-nutrient
  ]

  if threshold-monitored?
  [
    set information 1
    set color 15
    if agents-uniform? or houseowner-types != "none"
    [  ; immediate legislation when lake state is found to be undesirable
      set legislation 1
      set color 17

      set year-when-informed years
      if messages?
      [
        print (word "Msg "msg-counter": In year "years", the municipality publishes new rules on necessary oss upgrades.")
        set msg-counter msg-counter + 1
      ]
    ] ; otherwise it takes a year to act
  ]
end

to-report pike-loss-perception
  ;;; idle - Calculates and reports the perception of how much pike is lost relative to the pike-expectation level. Printed tickwise on the model interface.
  ;;; @report sup -  the relative perceived loss compared to the pike-expectation level
  let sup 0
  let loss pike-expectation - pike-stock
  if loss > 0
    [
      set sup loss ^ 2 / (loss ^ 2 + 0.75 ^ 2)
      if year-when-pike-became-critical = 0
      [set year-when-pike-became-critical years]
    ]
  report sup
end

to create-files
  ;;; Prepares the different log-files dependent on choices made from the interface.
  if experiment = "biggs-baseline"
  [set critical-nutrient target-nutrients]

  ifelse scenario = "Rtipping"
  [set path (word "simdata/"scenario"_Exp"experiment"_N" target-nutrients "_s" nutrient-change)]
  [set path (word "simdata/"scenario"_Scen"houseowner-types"_w" willingness-to-upgrade"_n0"init-nutrients"_n"critical-nutrient)]

  if log-ticks?
  [ ;   if   +str(ntarget[i])+'_s'+str(nspeed[j])+'_P1.5.csv
    ; Clear the test output file, write column headings
    ;set filename (word "simdata/"versuch"_N" init-nutrients "_B" initial-number-bream "_P" initial-number-pike ".csv") "_s" nutrient-change
    set filename (word path "_R" run-number".csv")
    if (file-exists? filename) [carefully [file-delete filename] [print error-message]]
    file-open filename
    file-type "tick,"
    file-type "bream,"
    file-type "pike,"
    file-type "nutrients,"
    file-type "vegetation,"
    file-type "year,"
    file-type "upgrade-efficiency,"
    file-type "upgraded households,"
    file-close
  ]

  if log-lake-per-year?
  [
    set filename-lake (word path"_R" run-number"_annual.csv")
    if (file-exists? filename-lake) [carefully [file-delete filename-lake] [print error-message]]
    file-open filename-lake
    file-type "year,"
    file-type "bream,"
    file-type "pike,"
    file-type "nutrients,"
    file-type "vegetation,"
    file-type "upgrade-efficiency,"
    file-type "upgraded households,"
    file-type "sewage water,"
    file-close
  ]

  if log-agents?
  [
    set filename-houseowners (word path"_agents_R" run-number".csv")
    if (file-exists? filename-houseowners) [carefully [file-delete filename-houseowners] [print error-message]]
    file-open filename-houseowners
    file-type "year,"
    file-type "id,"
    file-type "compliance,"
    file-type "time-lag,"
    file-type "oss,"
    file-close
  ]
end

to inform-households
  ;;; Based on phytoplankton abundance, the municipality informs the house owners on upgrade necessity.

   ask households with [oss = 0 and information = 0] ; they are informed just once
   [
     set information 1
     set time-lag-for-implementation 0
   ]
   if messages? [
     print (word "Msg "msg-counter": In year "years", tick "ticks", house owners were informed about necessary oss upgrade.")
     set msg-counter msg-counter + 1
   ]
end

to update-bream
  ;;; Translates bream value from system dynamics calculation to visualization as turtles on the interface
  ifelse bream-stock >= count whitefishs
  [; produce more bream
    ask n-of (bream-stock - count whitefishs) patches with [pxcor > coast-size][
      sprout-whitefishs 1 [
        set color white
        set size 1.5
        set label-color blue - 2
        ;set energy random (2 * cyprinid-gain-from-food)
      ]
    ]
  ]; reduce bream
  [
    ask n-of (count whitefishs - bream-stock) whitefishs
    [die]
  ]
end

to update-pike
  ;;; Translates pike value from system dynamics calculation to visualization as turtles on the interface
  ifelse pike-stock >= count pikes
  [ ; produce more pikes
    ask n-of (pike-stock - count pikes) patches with [pxcor > coast-size][
      sprout-pikes 1 [
        set color orange
        set size 3.0
      ]
    ]
  ]
  [ ; reduce pikes
    ask n-of (count pikes - pike-stock) pikes
    [die]
  ]
end

to update-vegetation
  ;;; Tranlsates vegetation value from system dynamics calculation to visualization as patches on the interface

  let difference round ((vegetation - green-part-of-lake) * number-of-lake-patches / 100)

  ifelse difference > 0
  [ ifelse green-part-of-lake = 100 [print (word "The lake is full of water plants")]
    [; increase patches of vegetation
    ask n-of difference lake-patches [
        set pcolor green
        ask self [set lake-patches other lake-patches]   ; remove this patch from lake patches
        ask self [set veg-patches (patch-set self veg-patches)]      ; add this patch to veg patches
      ]
    ]
  ]
  [ ; decrease patches of vegetation
    if difference < 0 ; if diff = 0, nothing to be done
    [
      ask n-of abs difference veg-patches [
        set pcolor blue
        ask self [set veg-patches other veg-patches]   ; remove this patch from veg patches
        ask self [set lake-patches (patch-set self lake-patches)]      ; add this patch to lake patches
      ]
    ]
  ]

  ;an advancement from profiling: replacing the following two statements by the above specification of adding or taking away individual patches
  ;set lake-patches patches with [pcolor = blue] ; patch-set
  ;set veg-patches patches with [pcolor = green] ; patch-set
end


; some report functions to feed into the system dynamics part

to-report sewage-water
  ;;; Linear sewage function based on number of affectors (house owners with OSS upgrade). Assumes an overall and linear nutrient reduction with < 50% house owners
  ;;; with old OSS, so that the overall lake nutrient concentration improves symmetrically as it would increase with > 50% of households having old OSS.
  ;;; @report nutrients-from-sewage
  ifelse not (nutrient-series = "dynamic") [
    report 0
  ]
  [ ifelse ticks mod 365 = 0
    [
      ifelse nutrients >= init-nutrients
      [
        let b max-sewage-water / (initial-number-households - tolerance-level-affectors)
        let a b * tolerance-level-affectors * -1
        let nutrients-from-sewage a + b * affectors
        ifelse nutrients + nutrients-from-sewage < init-nutrients ; in this case sewage recycling drops the nutrient level below initial
        [
          report init-nutrients - nutrients
        ]
        [
          report nutrients-from-sewage
        ]
      ]
      [
        report 0
      ]
    ]
    [report 0]
  ]
end

to-report constant-or-dynamic-nutrients
  ;;; Based on the chosen nutrient-series scenario (interface), nutrients become a constant or dynamic driver of bream-stock
  ;;; @report current dynamic value of nutrients or value from external nutrient series (transient-nutrients)
  ifelse nutrient-series = "constant"
  [ report transient-nutrients ] ; constant
  [ ifelse nutrient-series = "dynamic"
    [report nutrients]
    [report transient-nutrients]
  ]
end

to update-nutrients
  ;;; Linear function of nutrient in- or decrease.
  ifelse nutrient-series = "transient-up"
  [
    let a transient-nutrients + nutrient-change
    ifelse a > target-nutrients
    [set transient-nutrients target-nutrients]
    [set transient-nutrients a]
  ]
  [if nutrient-series = "transient-down" ; case transient-down
    [
      let a transient-nutrients - nutrient-change
      ifelse a < target-nutrients ; case transient-down
      [set transient-nutrients target-nutrients]
      [set transient-nutrients a]  ] ]
end

to update-settings
  ;;; Ipdates global variables/settings (nutrient-series) according to experiments specification, at times when necessary (see phase description)
  if experiment != "none"
  [
    let year floor (ticks / 365)
    ;phase 1: transient, static increase of nutrients, different speeds, same interval
    ifelse year = 11
      [
        if (experiment = "transient-hysteresis") or (experiment = "speed-to-tip")
        [set nutrient-series "transient-up"]
        if experiment = "transient-hysteresis-down"
        [set nutrient-series "transient-down"]
        if experiment = "biggs-baseline"
        [set nutrient-series "transient-up"]
      ]
      [if year > 11
        [; check wether nutrient level has reached target
           if ((nutrient-series = "transient-up") and ((precision transient-nutrients 2) >= target-nutrients)) or
           ((nutrient-series = "transient-down") and ((precision transient-nutrients 2) <= target-nutrients))
           [
           ifelse experiment = "biggs-baseline" and nutrient-series = "transient-up"
             [
              set nutrient-series "transient-down"
              set target-nutrients init-nutrients
             ]
             [set nutrient-series "constant"
              set time-to-finish 31
              set transient-nutrients target-nutrients
             ]
           ]

         ;phase 2: stable-state on final nutrient level, stop criterion
          set time-to-finish time-to-finish - 1
        ] ; if year > 11
      ] ; ifelse
  ] ; if experiment !=none
end

to update-settings-once
  ;;; updates global variables according to experiment choices
  if experiment != "none"
  [
    if experiment = "transient-hysteresis"
    [set initial-state "clear"
     set nutrient-series "constant"]
    if experiment = "transient-hysteresis-down"
    [set initial-state "turbid"
     set nutrient-series "constant" ]

    if experiment = "biggs-baseline"
    [set initial-state "clear"
     set nutrient-series "constant" ]

    ; set nutrient-series "constant" ; not sure when and why this was used
    if experiment = "speed-to-tip"
    [set target-nutrients 2.5]
  ]

  ifelse initial-state = "clear"
  [
    set init-nutrients 0.7
    set initial-number-pike 1.8
    set initial-number-bream 20
  ]
  [ ; turbid
    set init-nutrients 2.5
    set initial-number-pike 0.04
    set initial-number-bream 84
  ]

end

to-report bream-reproduction
  ;;; test - only for checking values against other output (SD)
  ;;; @report Number for bream growth
  report  bream-reproduction-rate * (constant-or-dynamic-nutrients / (constant-or-dynamic-nutrients + 0.5))
end

to-report biomanipulation-sd
  ;;; Calculates the bream reduction for a particular day depending on the effort (per year) and the number of days spent to do this
  ifelse days-to-catch-bream > 0
  [
    let bream-catch 0
    ;
    set bream-catch bream-this-year * reduction-rate / (100 * days-per-year)
    ; report bream reduction
    report bream-catch
  ]
  [ set biomanipulate-this-year false
    report 0
  ]
end

to-report green-part-of-lake
  ;;; Percentage of vegetation in patch-based representation.
  ;;; @report Percentage of vegetation patches compared to the whole lake
  ;report count patches with [ pcolor = green ] / (count patches with [ pcolor = blue ] + count patches with [ pcolor = green ])* 100
  report count veg-patches / number-of-lake-patches * 100
end

to save-state
  ;;; Saves a set of variables from just one time-step, triggered by user action on interface.
  set filename-once (word "simdata/"scenario"_Scen"houseowner-types"_w"willingness-to-upgrade"_n0"init-nutrients"_n"critical-nutrient"_once_R"run-number".csv")
  if (file-exists? filename-once) [carefully [file-delete filename-once] [print error-message]]
  file-open filename-once
     file-type "year,"
     file-type "yInformed,"
     file-type "yPikeCritical,"
     file-type "yNutrientsCritical,"
     file-type "yPikeBack,"
     file-type "yNutrientsBack,"
     file-print " "
     file-type years file-type ","
     file-type year-when-informed file-type ","
     file-type year-when-pike-became-critical file-type ","
     file-type year-when-nutrients-became-critical file-type ","
     file-type year-when-desired-pike-is-back file-type ","
     file-type year-when-desired-level-is-back file-type ","
  file-close
end

to system-dynamics-do-plots
  ;;; Updates the plots for SD numbers from fish and nutrients.
  set-current-plot "Fish"
  system-dynamics-do-plot
  set-current-plot "Nutrients"
  system-dynamics-do-plot
end
@#$#@#$#@
GRAPHICS-WINDOW
214
10
754
354
-1
-1
6.57
1
14
1
1
1
0
0
1
1
0
80
0
50
1
1
1
ticks
30.0

SLIDER
6
554
207
587
initial-number-bream
initial-number-bream
0
100
20.0
1
1
NIL
HORIZONTAL

SLIDER
6
586
207
619
initial-number-pike
initial-number-pike
0
10
1.8
0.1
1
NIL
HORIZONTAL

BUTTON
147
10
211
44
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
147
150
211
184
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
597
380
671
425
Bream
bream-stock
3
1
11

MONITOR
598
427
670
472
Pike
pike-stock
3
1
11

MONITOR
509
380
595
425
Vegetation %
green-part-of-lake
0
1
11

TEXTBOX
7
535
159
553
Ecosystem settings
11
0.0
0

SLIDER
16
422
198
455
initial-number-households
initial-number-households
0
100
16.0
1
1
NIL
HORIZONTAL

SWITCH
17
138
125
171
regulate?
regulate?
1
1
-1000

PLOT
768
10
1089
171
Fish
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"pike-stock" 1.0 0 -955883 true "" ""
"bream-stock" 1.0 0 -8275240 true "" ""
"vegetation" 1.0 0 -10899396 true "" ""

BUTTON
147
111
211
145
step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
553
706
752
739
user-predation-efficiency
user-predation-efficiency
0
1
0.1
0.1
1
NIL
HORIZONTAL

MONITOR
676
380
756
425
bream turtle
count whitefishs
17
1
11

MONITOR
677
426
756
471
pike turtle
count pikes
17
1
11

SLIDER
555
554
754
587
r-bream
r-bream
0
0.01
0.0075
0.0001
1
NIL
HORIZONTAL

SLIDER
554
635
753
668
predation-rate-pike
predation-rate-pike
0
0.1
0.05
0.01
1
NIL
HORIZONTAL

SLIDER
554
671
752
704
mortality-pike
mortality-pike
0
0.1
0.00225
0.01
1
NIL
HORIZONTAL

TEXTBOX
563
535
713
553
SD parameters
12
0.0
1

TEXTBOX
17
322
142
340
Houseowners
11
0.0
1

MONITOR
598
480
755
525
Effective bream reproduction
bream-reproduction
3
1
11

PLOT
1088
10
1408
171
Nutrients
NIL
NIL
0.0
2.0
0.0
10.0
true
true
"" ""
PENS
"nutrients" 1.0 0 -16777216 true "" ""
"nutrients_ext" 1.0 0 -7500403 true "" "plot constant-or-dynamic-nutrients"

TEXTBOX
5
105
130
123
Scenario settings
12
0.0
1

MONITOR
508
426
596
471
SD Vegetation
vegetation
3
1
11

SLIDER
555
589
754
622
competition-bream
competition-bream
0
0.001
7.0E-5
0.00001
1
NIL
HORIZONTAL

SLIDER
553
739
752
772
competition-pike
competition-pike
0
0.1
2.75E-4
0.01
1
NIL
HORIZONTAL

SLIDER
7
779
179
812
init-nutrients
init-nutrients
0.1
3.5
0.7
0.1
1
NIL
HORIZONTAL

SLIDER
7
815
180
848
recycling-rate
recycling-rate
0
0.5
0.05
0.05
1
NIL
HORIZONTAL

SLIDER
7
853
181
886
max-sewage-water
max-sewage-water
0
0.5
0.1
0.005
1
NIL
HORIZONTAL

TEXTBOX
11
668
161
686
Pollution settings
12
0.0
1

MONITOR
440
380
506
425
Nutrients
constant-or-dynamic-nutrients
3
1
11

MONITOR
220
376
292
421
Affectors
affectors
17
1
11

MONITOR
295
376
366
421
Sewage
sewage-water
17
1
11

BUTTON
147
48
211
81
10 years
repeat 3650 [go]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
5
10
134
70
scenario
Rtipping
1
0
String

BUTTON
147
79
211
112
1 year
repeat 365 [go]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
6
733
179
778
nutrient-series
nutrient-series
"constant" "dynamic" "transient-up" "transient-down"
0

CHOOSER
181
733
323
778
nutrient-change
nutrient-change
0.05 0.1 0.15 0.2
1

PLOT
768
169
1089
326
Vegetation
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"vegetation" 1.0 0 -15302303 true "" "plot green-part-of-lake"

SLIDER
17
252
200
285
pike-expectation
pike-expectation
0
2.5
1.7
0.1
1
NIL
HORIZONTAL

PLOT
767
337
1003
487
Perception of pike loss
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot pike-loss-perception"

PLOT
767
486
1003
636
Upgraded households
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -955883 true "" "plot count households with [oss = 1]"

SLIDER
181
780
320
813
target-nutrients
target-nutrients
0
2.5
2.4
0.1
1
NIL
HORIZONTAL

CHOOSER
7
686
148
731
experiment
experiment
"none" "biggs-baseline" "lake-states" "speed-to-tip" "transient-hysteresis" "transient-hysteresis-down"
4

CHOOSER
150
686
288
731
initial-state
initial-state
"clear" "turbid"
0

MONITOR
162
330
212
375
Year
years
1
1
11

PLOT
1217
487
1403
635
Upgrade efficiency
NIL
NIL
0.0
10.0
0.0
100.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot upgrade-efficiency"

SWITCH
16
342
151
375
agents-uniform?
agents-uniform?
1
1
-1000

TEXTBOX
17
122
167
140
Municipality
11
0.0
1

SLIDER
16
454
197
487
willingness-to-upgrade
willingness-to-upgrade
0
1
0.2
0.1
1
NIL
HORIZONTAL

CHOOSER
16
377
151
422
houseowner-types
houseowner-types
"homoOwners" "homoOwnersSocial" "homoOwnersEnforced" "uniform"
0

PLOT
1003
486
1217
636
Willingness-to-update
NIL
NIL
0.0
1.0
0.0
10.0
true
false
"" ""
PENS
"comp" 0.1 1 -16777216 true "" "histogram [compliance] of households"

SLIDER
16
486
197
519
radius-of-neighbors
radius-of-neighbors
0
5
3.0
1
1
NIL
HORIZONTAL

PLOT
1003
338
1262
487
Number of households informed
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count households with [information = 1]"

INPUTBOX
187
827
263
887
run-number
0.0
1
0
Number

MONITOR
220
506
435
551
Year when threshold identified:
year-when-informed
2
1
11

MONITOR
220
550
435
595
Year when pike became critical:
year-when-pike-became-critical
17
1
11

MONITOR
220
593
435
638
Year when nutrients became critical
year-when-nutrients-became-critical
2
1
11

INPUTBOX
135
191
200
251
critical-nutrient
4.0
1
0
Number

SLIDER
17
285
200
318
tolerance-level-affectors
tolerance-level-affectors
0
100
50.0
1
1
NIL
HORIZONTAL

PLOT
1088
169
1408
326
Sewage water
NIL
NIL
0.0
10.0
-0.2
0.2
true
true
"" ""
PENS
"sewage" 1.0 0 -16777216 true "" "plot sewage-water"

MONITOR
220
462
435
507
Year when level is back to normal:
year-when-desired-level-is-back
2
1
11

SWITCH
17
172
125
205
respond-direct?
respond-direct?
0
1
-1000

CHOOSER
17
205
125
250
threshold-variable
threshold-variable
"pike" "nutrients"
1

BUTTON
370
643
435
676
save
save-state
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
220
419
435
464
Year when pike is back:
year-when-desired-pike-is-back
2
1
11

SWITCH
370
684
494
717
log-ticks?
log-ticks?
0
1
-1000

SWITCH
370
748
511
781
log-agents?
log-agents?
1
1
-1000

SWITCH
370
780
525
813
log-laststate?
log-laststate?
1
1
-1000

SWITCH
370
716
555
749
log-lake-per-year?
log-lake-per-year?
1
1
-1000

SWITCH
438
644
555
677
messages?
messages?
1
1
-1000

SWITCH
370
813
491
846
profiling?
profiling?
1
1
-1000

SLIDER
6
617
207
650
initial-vegetation
initial-vegetation
0
100
57.0
1
1
NIL
HORIZONTAL

SLIDER
1038
770
1239
803
bream-reduction-per-year
bream-reduction-per-year
0
100
0.0
1
1
NIL
HORIZONTAL

SLIDER
553
778
752
811
r-vegetation
r-vegetation
0.001
0.01
0.007
0.001
1
NIL
HORIZONTAL

SLIDER
553
811
752
844
veg-mortality
veg-mortality
0.001
0.01
0.007
0.001
1
NIL
HORIZONTAL

TEXTBOX
1039
745
1189
763
Biomanipulation
12
0.0
1

SLIDER
1038
805
1239
838
days-per-year
days-per-year
0
365
0.0
1
1
NIL
HORIZONTAL

SLIDER
553
843
752
876
competition-vegetation
competition-vegetation
0.00001
0.0001
6.0E-5
0.00001
1
NIL
HORIZONTAL

PLOT
767
644
1004
801
bream-catch
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot biomanipulation-sd"

SWITCH
1038
840
1204
873
biomanipulate?
biomanipulate?
1
1
-1000

@#$#@#$#@
## WHAT IS IT?

LimnoSES is a coupled system dynamics, agent-based model to simulate social-ecological feedbacks in lake use and management. The focus lies on shallow lakes where ecological regime shifts can occur between the turbid and the clear water state. We provide a regime shift evaluation that includes social responses and regulation mechanisms of important drivers in the lake. In particular, the coordination among private house owners with insufficient sewage water systems and the regulating municipality play an important role since it causes time lags of social responses to changes in the lake state.

## HOW IT WORKS

* Lake dynamics are implemented in the system dynamics module, the presented fish agents are just for visualization purposes and have no individual characteristics. The section of sliders "SD Parameters" belongs to it and their values are not supposed to be changed as they link to the minimal model by Scheffer (1989).  
* Lake dynamics run on daily basis, with one tick per day. Social processes run on an annual basis.
* House owners are initialized with a (up to now homogeneous) value for their willingness-to-upgrade. 'Houseowner-types' determines which scenario of regulation will be applied to them.
** homoOwners - no reinforcment 
** homoOwnersSocial - social pressure, means that one house owner doing the upgrade improves the willingness-to-upgrade from its neighbors
** homoOwnersEnforced - central enforcement, means that after a constant time lag, the municipality sends out inspectors to increase the individuals willingness-to-upgrade

## THINGS TO NOTICE


## HOW TO USE IT
Single simulations and tests through the user interface:

* To verify that particular nutrient levels trigger a shift in the fish populations, one can simulate lake dynamics from initially either the clear or turbid state (under "Pollution settings"). The nutrient change over time can be set via the chooser "nutrient-series". When it is set on 'dynamic', nutrient change is derived from the connected households with insufficiently treated sewage water. The 'Fish' and 'Nutrients' plot show the development over time. 

## THINGS TO TRY
Experiments that are available through the behavior space:

* Rtipping_demo: Evaluates externally driven nutrient changes at different speeds and up to different levels. Demonstrates that transient dynamics of nutrient change can trigger regime shifts dependending on the speed.
* TurbidityResponse_demo: Evaluates the response from house owners in terms of their time lag for upgrading the private sewage system. This is done for different regulation pathways (no reinforcement, social pressure, or central enforcement). 

As each of those experiments evaluate only one side of the social-ecological model, some experiments for the fully coupled model will follow. 

## EXTENDING THE MODEL
under development

## CREDITS AND REFERENCES
LimnoSES was developed by Romina Martin together with Maja Schlüter during the Biodiversa project "LimnoTip", 2013-2015, at the Stockholm Resilience Centre, Sweden. Check  https://www.openabm.org/model/5292/version/1/view for updates and accompanying information such as the ODD+D description.

* Martin, R. & Schlüter, M. Combining system dynamics and agent-based modeling to analyze social-ecological interactions—an example from modeling restoration of a shallow lake. Front. Environ. Sci. 3, 1–15 (2015).

* Scheffer, M. Alternative stable states in eutrophic, shallow freshwater systems: A minimal model. Hydrobiol. Bull. 23, 73–83 (1989). 
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

boat
false
0
Polygon -1 true false 63 162 90 207 223 207 290 162
Rectangle -6459832 true false 150 32 157 162
Polygon -13345367 true false 150 34 131 49 145 47 147 48 149 49
Polygon -7500403 true true 158 33 230 157 182 150 169 151 157 156
Polygon -7500403 true true 149 55 88 143 103 139 111 136 117 139 126 145 130 147 139 147 146 146 149 55

boat 3
false
0
Polygon -1 true false 63 162 90 207 223 207 290 162
Rectangle -6459832 true false 150 32 157 162
Polygon -13345367 true false 150 34 131 49 145 47 147 48 149 49
Polygon -7500403 true true 158 37 172 45 188 59 202 79 217 109 220 130 218 147 204 156 158 156 161 142 170 123 170 102 169 88 165 62
Polygon -7500403 true true 149 66 142 78 139 96 141 111 146 139 148 147 110 147 113 131 118 106 126 71

boat2
false
15
Polygon -1 true true 75 165 90 207 223 207 285 165
Circle -6459832 true false 90 90 30
Rectangle -6459832 true false 90 120 120 165
Line -6459832 false 90 135 30 120
Line -6459832 false 30 120 15 195

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

crab
true
0
Polygon -1184463 true false 216 204 241 233 247 254 229 266 216 252 194 210
Polygon -1184463 true false 195 90 225 75 245 75 255 60 225 45 255 90 240 105 225 105 210 105
Polygon -1184463 true false 105 90 75 75 45 45 75 30 60 60 45 90 60 105 75 105 90 105
Polygon -2674135 true false 130 84 132 63 105 50 106 16 148 1 190 17 190 51 167 64 170 86
Polygon -1184463 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -955883 true false 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99
Circle -16777216 false false 122 24 16
Circle -16777216 false false 151 24 19
Circle -16777216 true false 121 24 17
Circle -16777216 true false 150 23 20

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

fish 3
false
0
Polygon -7500403 true true 137 105 124 83 103 76 77 75 53 104 47 136
Polygon -7500403 true true 226 194 223 229 207 243 178 237 169 203 167 175
Polygon -7500403 true true 137 195 124 217 103 224 77 225 53 196 47 164
Polygon -7500403 true true 40 123 32 109 16 108 0 130 0 151 7 182 23 190 40 179 47 145
Polygon -7500403 true true 45 120 90 105 195 90 275 120 294 152 285 165 293 171 270 195 210 210 150 210 45 180
Circle -1184463 true false 244 128 26
Circle -16777216 true false 248 135 14
Line -16777216 false 48 121 133 96
Line -16777216 false 48 179 133 204
Polygon -7500403 true true 241 106 241 77 217 71 190 75 167 99 182 125
Line -16777216 false 226 102 158 95
Line -16777216 false 171 208 225 205
Polygon -1 true false 252 111 232 103 213 132 210 165 223 193 229 204 247 201 237 170 236 137
Polygon -1 true false 135 98 140 137 135 204 154 210 167 209 170 176 160 156 163 126 171 117 156 96
Polygon -16777216 true false 192 117 171 118 162 126 158 148 160 165 168 175 188 183 211 186 217 185 206 181 172 171 164 156 166 133 174 121
Polygon -1 true false 40 121 46 147 42 163 37 179 56 178 65 159 67 128 59 116

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

house bungalow
false
0
Rectangle -7500403 true true 210 75 225 255
Rectangle -7500403 true true 90 135 210 255
Rectangle -16777216 true false 165 195 195 255
Line -16777216 false 210 135 210 255
Rectangle -16777216 true false 105 202 135 240
Polygon -7500403 true true 225 150 75 150 150 75
Line -16777216 false 75 150 225 150
Line -16777216 false 195 120 225 150
Polygon -16777216 false false 165 195 150 195 180 165 210 195
Rectangle -16777216 true false 135 105 165 135

house colonial
false
0
Rectangle -7500403 true true 270 75 285 255
Rectangle -7500403 true true 45 135 270 255
Rectangle -16777216 true false 124 195 187 256
Rectangle -16777216 true false 60 195 105 240
Rectangle -16777216 true false 60 150 105 180
Rectangle -16777216 true false 210 150 255 180
Line -16777216 false 270 135 270 255
Polygon -7500403 true true 30 135 285 135 240 90 75 90
Line -16777216 false 30 135 285 135
Line -16777216 false 255 105 285 135
Line -7500403 true 154 195 154 255
Rectangle -16777216 true false 210 195 255 240
Rectangle -16777216 true false 135 150 180 180

house efficiency
false
0
Rectangle -7500403 true true 180 90 195 195
Rectangle -7500403 true true 90 165 210 255
Rectangle -16777216 true false 165 195 195 255
Rectangle -16777216 true false 105 202 135 240
Polygon -7500403 true true 225 165 75 165 150 90
Line -16777216 false 75 165 225 165

house municipality
false
0
Rectangle -7500403 true true 270 75 285 255
Rectangle -7500403 true true 45 135 270 255
Rectangle -16777216 true false 124 195 180 255
Rectangle -16777216 true false 60 195 105 240
Rectangle -16777216 true false 60 150 105 180
Rectangle -16777216 true false 210 150 255 180
Line -16777216 false 270 135 270 255
Polygon -7500403 true true 30 135 285 135 240 90 75 90
Line -16777216 false 30 135 285 135
Line -16777216 false 255 105 285 135
Line -7500403 true 154 195 154 255
Rectangle -16777216 true false 210 195 255 240
Circle -1 true false 129 144 42
Circle -16777216 true false 135 150 30
Line -1 false 150 180 150 150
Line -1 false 135 165 165 165
Line -1 false 135 150 165 180
Line -1 false 165 150 135 180

house stuga
false
0
Rectangle -2674135 true false 150 30 165 120
Rectangle -2674135 true false 45 90 165 195
Rectangle -16777216 true false 120 120 150 195
Rectangle -16777216 true false 60 127 90 165
Polygon -2674135 true false 180 90 30 90 105 15
Line -16777216 false 30 90 180 90
Polygon -7500403 true true 240 150 210 135 165 225 180 225 210 180 210 225 195 300 225 300 240 225 255 300 285 300 270 225 270 180 300 225 315 225 270 135
Circle -7500403 true true 210 75 60

house two story
false
0
Polygon -7500403 true true 2 180 227 180 152 150 32 150
Rectangle -7500403 true true 270 75 285 255
Rectangle -7500403 true true 75 135 270 255
Rectangle -16777216 true false 124 195 187 256
Rectangle -16777216 true false 210 195 255 240
Rectangle -16777216 true false 90 150 135 180
Rectangle -16777216 true false 210 150 255 180
Line -16777216 false 270 135 270 255
Rectangle -7500403 true true 15 180 75 255
Polygon -7500403 true true 60 135 285 135 240 90 105 90
Line -16777216 false 75 135 75 180
Rectangle -16777216 true false 30 195 93 240
Line -16777216 false 60 135 285 135
Line -16777216 false 255 105 285 135
Line -16777216 false 0 180 75 180
Line -7500403 true 60 195 60 240
Line -7500403 true 154 195 154 255

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

person business
false
0
Rectangle -1 true false 120 90 180 180
Polygon -13345367 true false 135 90 150 105 135 180 150 195 165 180 150 105 165 90
Polygon -7500403 true true 120 90 105 90 60 195 90 210 116 154 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 183 153 210 210 240 195 195 90 180 90 150 165
Circle -7500403 true true 110 5 80
Rectangle -7500403 true true 127 76 172 91
Line -16777216 false 172 90 161 94
Line -16777216 false 128 90 139 94
Polygon -13345367 true false 195 225 195 300 270 270 270 195
Rectangle -13791810 true false 180 225 195 300
Polygon -14835848 true false 180 226 195 226 270 196 255 196
Polygon -13345367 true false 209 202 209 216 244 202 243 188
Line -16777216 false 180 90 150 165
Line -16777216 false 120 90 150 165

person construction
false
0
Rectangle -7500403 true true 123 76 176 95
Polygon -1 true false 105 90 60 195 90 210 115 162 184 163 210 210 240 195 195 90
Polygon -13345367 true false 180 195 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285
Circle -7500403 true true 110 5 80
Line -16777216 false 148 143 150 196
Rectangle -16777216 true false 116 186 182 198
Circle -1 true false 152 143 9
Circle -1 true false 152 166 9
Rectangle -16777216 true false 179 164 183 186
Polygon -955883 true false 180 90 195 90 195 165 195 195 150 195 150 120 180 90
Polygon -955883 true false 120 90 105 90 105 165 105 195 150 195 150 120 120 90
Rectangle -16777216 true false 135 114 150 120
Rectangle -16777216 true false 135 144 150 150
Rectangle -16777216 true false 135 174 150 180
Polygon -955883 true false 105 42 111 16 128 2 149 0 178 6 190 18 192 28 220 29 216 34 201 39 167 35
Polygon -6459832 true false 54 253 54 238 219 73 227 78
Polygon -16777216 true false 15 285 15 255 30 225 45 225 75 255 75 270 45 285

person farmer
false
0
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Polygon -1 true false 60 195 90 210 114 154 120 195 180 195 187 157 210 210 240 195 195 90 165 90 150 105 150 150 135 90 105 90
Circle -7500403 true true 110 5 80
Rectangle -7500403 true true 127 79 172 94
Polygon -13345367 true false 120 90 120 180 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 180 90 172 89 165 135 135 135 127 90
Polygon -6459832 true false 116 4 113 21 71 33 71 40 109 48 117 34 144 27 180 26 188 36 224 23 222 14 178 16 167 0
Line -16777216 false 225 90 270 90
Line -16777216 false 225 15 225 90
Line -16777216 false 270 15 270 90
Line -16777216 false 247 15 247 90
Rectangle -6459832 true false 240 90 255 300

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

plant medium
false
0
Rectangle -7500403 true true 135 165 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 165 120 120 150 90 180 120 165 165

plant small
false
0
Rectangle -7500403 true true 135 240 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 240 120 195 150 165 180 195 165 240

shark
false
0
Polygon -7500403 true true 283 153 288 149 271 146 301 145 300 138 247 119 190 107 104 117 54 133 39 134 10 99 9 112 19 142 9 175 10 185 40 158 69 154 64 164 80 161 86 156 132 160 209 164
Polygon -7500403 true true 199 161 152 166 137 164 169 154
Polygon -7500403 true true 188 108 172 83 160 74 156 76 159 97 153 112
Circle -16777216 true false 256 129 12
Line -16777216 false 222 134 222 150
Line -16777216 false 217 134 217 150
Line -16777216 false 212 134 212 150
Polygon -7500403 true true 78 125 62 118 63 130
Polygon -7500403 true true 121 157 105 161 101 156 106 152

sheep
false
0
Rectangle -16777216 true false 166 225 195 285
Rectangle -16777216 true false 62 225 90 285
Rectangle -7500403 true true 30 75 210 225
Circle -7500403 true true 135 75 150
Circle -16777216 true false 180 76 116

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tile water
false
0
Rectangle -7500403 true true -1 0 299 300
Polygon -1 true false 105 259 180 290 212 299 168 271 103 255 32 221 1 216 35 234
Polygon -1 true false 300 161 248 127 195 107 245 141 300 167
Polygon -1 true false 0 157 45 181 79 194 45 166 0 151
Polygon -1 true false 179 42 105 12 60 0 120 30 180 45 254 77 299 93 254 63
Polygon -1 true false 99 91 50 71 0 57 51 81 165 135
Polygon -1 true false 194 224 258 254 295 261 211 221 144 199

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -6459832 true false 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Rectangle -7500403 true true 195 106 285 150
Rectangle -7500403 true true 195 90 255 105
Polygon -7500403 true true 240 90 217 44 196 90
Polygon -16777216 true false 234 89 218 59 203 89
Rectangle -1 true false 240 93 252 105
Rectangle -16777216 true false 242 96 249 104
Rectangle -16777216 true false 241 125 285 139
Polygon -1 true false 285 125 277 138 269 125
Polygon -1 true false 269 140 262 125 256 140
Rectangle -7500403 true true 45 120 195 195
Rectangle -7500403 true true 45 114 185 120
Rectangle -7500403 true true 165 195 180 270
Rectangle -7500403 true true 60 195 75 270
Polygon -7500403 true true 45 105 15 30 15 75 45 150 60 120

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.2
@#$#@#$#@
setup
set grass? true
repeat 75 [ go ]
@#$#@#$#@
1.0
    org.nlogo.sdm.gui.AggregateDrawing 73
        org.nlogo.sdm.gui.StockFigure "attributes" "attributes" 1 "FillColor" "Color" 225 225 182 433 373 60 40
            org.nlogo.sdm.gui.WrappedStock "bream-stock" "initial-number-bream" 1
        org.nlogo.sdm.gui.StockFigure "attributes" "attributes" 1 "FillColor" "Color" 225 225 182 645 220 60 40
            org.nlogo.sdm.gui.WrappedStock "pike-stock" "initial-number-pike" 1
        org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 281 378 30 30
        org.nlogo.sdm.gui.RateConnection 3 311 393 366 392 421 392 NULL NULL 0 0 0
            org.jhotdraw.figures.ChopEllipseConnector REF 5
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.nlogo.sdm.gui.WrappedRate "immigration + bream-stock * bream-reproduction-rate * nutrient-impact / (nutrient-impact + H1)" "bream-reproduce"
                org.nlogo.sdm.gui.WrappedReservoir  REF 2 0
        org.nlogo.sdm.gui.BindingConnection 2 421 392 366 392 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.nlogo.sdm.gui.ChopRateConnector REF 6
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 227 318 50 50
            org.nlogo.sdm.gui.WrappedConverter "r-bream" "bream-reproduction-rate"
        org.nlogo.sdm.gui.BindingConnection 2 269 350 366 392 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 14
            org.nlogo.sdm.gui.ChopRateConnector REF 6
        org.nlogo.sdm.gui.RateConnection 3 505 389 598 383 691 377 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.jhotdraw.figures.ChopEllipseConnector
                org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 690 361 30 30
            org.nlogo.sdm.gui.WrappedRate "bream-stock ^ 2 * cb + \npredation-rate * pike-stock * bream-predation" "bream-die" REF 2
                org.nlogo.sdm.gui.WrappedReservoir  0   REF 22
        org.nlogo.sdm.gui.BindingConnection 2 505 389 598 383 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.nlogo.sdm.gui.ChopRateConnector REF 19
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 516 312 50 50
            org.nlogo.sdm.gui.WrappedConverter "predation-rate-pike" "predation-rate"
        org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 517 225 30 30
        org.nlogo.sdm.gui.RateConnection 3 547 240 590 240 633 240 NULL NULL 0 0 0
            org.jhotdraw.figures.ChopEllipseConnector REF 30
            org.jhotdraw.standard.ChopBoxConnector REF 3
            org.nlogo.sdm.gui.WrappedRate "immigration + \npike-stock * predation-rate * predation-efficiency * bream-predation * vegetation / (vegetation + H2)" "pike-repro"
                org.nlogo.sdm.gui.WrappedReservoir  REF 4 0
        org.nlogo.sdm.gui.RateConnection 3 717 237 767 235 818 233 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 3
            org.jhotdraw.figures.ChopEllipseConnector
                org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 817 217 30 30
            org.nlogo.sdm.gui.WrappedRate "pike-stock * pike-mortality + \nc-pike * pike-stock ^ 2" "pike-die" REF 4
                org.nlogo.sdm.gui.WrappedReservoir  0   REF 39
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 521 170 50 50
            org.nlogo.sdm.gui.WrappedConverter "user-predation-efficiency" "predation-efficiency"
        org.nlogo.sdm.gui.BindingConnection 2 558 207 590 240 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 42
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 633 240 590 240 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 3
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 549 320 590 240 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 28
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 554 348 598 383 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 28
            org.nlogo.sdm.gui.ChopRateConnector REF 19
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 792 138 50 50
            org.nlogo.sdm.gui.WrappedConverter "mortality-pike" "pike-mortality"
        org.nlogo.sdm.gui.BindingConnection 2 806 177 767 235 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 56
            org.nlogo.sdm.gui.ChopRateConnector REF 36
        org.nlogo.sdm.gui.BindingConnection 2 717 237 767 235 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 3
            org.nlogo.sdm.gui.ChopRateConnector REF 36
        org.nlogo.sdm.gui.BindingConnection 2 489 361 590 240 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 657 272 598 383 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 3
            org.nlogo.sdm.gui.ChopRateConnector REF 19
        org.nlogo.sdm.gui.StockFigure "attributes" "attributes" 1 "FillColor" "Color" 225 225 182 315 531 60 40
            org.nlogo.sdm.gui.WrappedStock "nutrients" "init-nutrients" 1
        org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 211 536 30 30
        org.nlogo.sdm.gui.RateConnection 3 241 551 272 551 303 551 NULL NULL 0 0 0
            org.jhotdraw.figures.ChopEllipseConnector REF 72
            org.jhotdraw.standard.ChopBoxConnector REF 70
            org.nlogo.sdm.gui.WrappedRate "recycling-rate * nutrients + sewage-water" "sewage"
                org.nlogo.sdm.gui.WrappedReservoir  REF 71 0
        org.nlogo.sdm.gui.RateConnection 3 387 550 450 550 514 550 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 70
            org.jhotdraw.figures.ChopEllipseConnector
                org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 513 535 30 30
            org.nlogo.sdm.gui.WrappedRate "recycling-rate * nutrients" "losses" REF 71
                org.nlogo.sdm.gui.WrappedReservoir  0   REF 81
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 208 22 50 50
            org.nlogo.sdm.gui.WrappedConverter "20" "H3"
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 638 404 50 50
            org.nlogo.sdm.gui.WrappedConverter "competition-bream" "cb"
        org.nlogo.sdm.gui.BindingConnection 2 648 418 598 383 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 86
            org.nlogo.sdm.gui.ChopRateConnector REF 19
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 702 303 50 50
            org.nlogo.sdm.gui.WrappedConverter "bream-stock ^ 2 / (bream-stock ^ 2 + H4 ^ 2)" "bream-predation"
        org.nlogo.sdm.gui.BindingConnection 2 505 382 706 332 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.jhotdraw.contrib.ChopDiamondConnector REF 91
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 748 348 50 50
            org.nlogo.sdm.gui.WrappedConverter "15" "H4"
        org.nlogo.sdm.gui.BindingConnection 2 760 360 739 340 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 96
            org.jhotdraw.contrib.ChopDiamondConnector REF 91
        org.nlogo.sdm.gui.BindingConnection 2 711 318 590 240 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 91
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 816 281 50 50
            org.nlogo.sdm.gui.WrappedConverter "competition-pike" "c-pike"
        org.nlogo.sdm.gui.BindingConnection 2 828 293 767 235 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 104
            org.nlogo.sdm.gui.ChopRateConnector REF 36
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 310 447 50 50
            org.nlogo.sdm.gui.WrappedConverter "0.5" "H1"
        org.nlogo.sdm.gui.BindingConnection 2 341 453 366 392 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 109
            org.nlogo.sdm.gui.ChopRateConnector REF 6
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 421 272 50 50
            org.nlogo.sdm.gui.WrappedConverter "0.00002" "immigration"
        org.nlogo.sdm.gui.BindingConnection 2 463 289 590 240 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 114
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 434 310 366 392 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 114
            org.nlogo.sdm.gui.ChopRateConnector REF 6
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 420 193 50 50
            org.nlogo.sdm.gui.WrappedConverter "11" "H2"
        org.nlogo.sdm.gui.BindingConnection 2 466 221 590 240 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 122
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 709 335 598 383 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 91
            org.nlogo.sdm.gui.ChopRateConnector REF 19
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 365 451 50 50
            org.nlogo.sdm.gui.WrappedConverter "constant-or-dynamic-nutrients" "nutrient-impact"
        org.nlogo.sdm.gui.BindingConnection 2 384 456 366 392 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 130
            org.nlogo.sdm.gui.ChopRateConnector REF 6
        org.nlogo.sdm.gui.BindingConnection 2 364 519 380 491 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 70
            org.jhotdraw.contrib.ChopDiamondConnector REF 130
        org.nlogo.sdm.gui.BindingConnection 2 387 550 450 550 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 70
            org.nlogo.sdm.gui.ChopRateConnector REF 78
        org.nlogo.sdm.gui.BindingConnection 2 303 551 272 551 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 70
            org.nlogo.sdm.gui.ChopRateConnector REF 73
        org.nlogo.sdm.gui.StockFigure "attributes" "attributes" 1 "FillColor" "Color" 225 225 182 170 124 60 40
            org.nlogo.sdm.gui.WrappedStock "vegetation" "initial-vegetation" 0
        org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 -4 131 30 30
        org.nlogo.sdm.gui.RateConnection 3 26 146 92 145 158 144 NULL NULL 0 0 0
            org.jhotdraw.figures.ChopEllipseConnector REF 146
            org.jhotdraw.standard.ChopBoxConnector REF 144
            org.nlogo.sdm.gui.WrappedRate "v-reproduction-rate * vegetation" "vegetation-reproduce"
                org.nlogo.sdm.gui.WrappedReservoir  REF 145 0
        org.nlogo.sdm.gui.RateConnection 3 242 145 318 148 395 152 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 144
            org.jhotdraw.figures.ChopEllipseConnector
                org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 394 137 30 30
            org.nlogo.sdm.gui.WrappedRate "vegetation ^ 2 * cV + \nv-mortality * ( vegetation * bream-stock ^ 2 / ( H3 ^ 2 + bream-stock ^ 2 ))" "vegetation-die" REF 145
                org.nlogo.sdm.gui.WrappedReservoir  0   REF 155
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 110 184 50 50
            org.nlogo.sdm.gui.WrappedConverter "r-vegetation" "v-reproduction-rate"
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 290 24 50 50
            org.nlogo.sdm.gui.WrappedConverter "veg-mortality" "v-mortality"
        org.nlogo.sdm.gui.BindingConnection 2 124 194 92 145 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 158
            org.nlogo.sdm.gui.ChopRateConnector REF 147
        org.nlogo.sdm.gui.BindingConnection 2 315 73 318 148 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 160
            org.nlogo.sdm.gui.ChopRateConnector REF 152
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 379 73 50 50
            org.nlogo.sdm.gui.WrappedConverter "competition-vegetation" "cV"
        org.nlogo.sdm.gui.BindingConnection 2 388 107 318 148 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 168
            org.nlogo.sdm.gui.ChopRateConnector REF 152
        org.nlogo.sdm.gui.BindingConnection 2 244 60 318 148 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 84
            org.nlogo.sdm.gui.ChopRateConnector REF 152
        org.nlogo.sdm.gui.BindingConnection 2 242 154 590 240 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 144
            org.nlogo.sdm.gui.ChopRateConnector REF 31
        org.nlogo.sdm.gui.BindingConnection 2 444 361 318 148 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.nlogo.sdm.gui.ChopRateConnector REF 152
        org.nlogo.sdm.gui.BindingConnection 2 158 144 92 145 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 144
            org.nlogo.sdm.gui.ChopRateConnector REF 147
        org.nlogo.sdm.gui.BindingConnection 2 242 145 318 148 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 144
            org.nlogo.sdm.gui.ChopRateConnector REF 152
        org.nlogo.sdm.gui.ConverterFigure "attributes" "attributes" 1 "FillColor" "Color" 130 188 183 639 477 50 50
            org.nlogo.sdm.gui.WrappedConverter "bream-reduction-per-year" "reduction-rate"
        org.nlogo.sdm.gui.RateConnection 3 495 425 540 468 585 511 NULL NULL 0 0 0
            org.jhotdraw.standard.ChopBoxConnector REF 1
            org.jhotdraw.figures.ChopEllipseConnector
                org.nlogo.sdm.gui.ReservoirFigure "attributes" "attributes" 1 "FillColor" "Color" 192 192 192 580 505 30 30
            org.nlogo.sdm.gui.WrappedRate "biomanipulation-sd" "biomanipulation" REF 2
                org.nlogo.sdm.gui.WrappedReservoir  0   REF 193
        org.nlogo.sdm.gui.BindingConnection 2 644 496 540 468 NULL NULL 0 0 0
            org.jhotdraw.contrib.ChopDiamondConnector REF 188
            org.nlogo.sdm.gui.ChopRateConnector REF 190
@#$#@#$#@
<experiments>
  <experiment name="Rtipping_demoA" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;transient-hysteresis&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="16"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-agents?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-laststate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;constant&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="run-number">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;Rtipping&quot;"/>
    </enumeratedValueSet>
    <steppedValueSet variable="target-nutrients" first="0.4" step="0.4" last="2.4"/>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="TurbidityResponse_demo" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10000"/>
    <enumeratedValueSet variable="bream-pike-half-saturation">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bream-vegetation-half-depletion">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="houseowner-types">
      <value value="&quot;homoOwners&quot;"/>
      <value value="&quot;homoOwnersSocial&quot;"/>
      <value value="&quot;homoOwnersEnforced&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="84"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="0.04"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-state">
      <value value="&quot;turbid&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-agents?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-laststate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;dynamic&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius-of-neighbors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="true"/>
    </enumeratedValueSet>
    <steppedValueSet variable="run-number" first="1" step="1" last="5"/>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;SewageControl&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="target-nutrients">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="willingness-to-upgrade" first="0.1" step="0.1" last="0.4"/>
  </experiment>
  <experiment name="FullRestorationNavRS" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="36500"/>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="bream-pike-half-saturation">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bream-vegetation-half-depletion">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical-nutrient">
      <value value="2"/>
      <value value="2.3"/>
      <value value="2.4"/>
      <value value="2.5"/>
      <value value="2.7"/>
      <value value="3"/>
      <value value="3.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="houseowner-types">
      <value value="&quot;homoOwners&quot;"/>
      <value value="&quot;homoOwnersEnforced&quot;"/>
      <value value="&quot;homoOwnersSocial&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="1.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-state">
      <value value="&quot;clear&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;dynamic&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius-of-neighbors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="respond-direct?">
      <value value="true"/>
    </enumeratedValueSet>
    <steppedValueSet variable="run-number" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="target-nutrients">
      <value value="2.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="threshold-variable">
      <value value="&quot;nutrients&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tolerance-level-affectors">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;SewageDRestore&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="willingness-to-upgrade">
      <value value="0.1"/>
      <value value="0.18"/>
      <value value="0.2"/>
      <value value="0.22"/>
      <value value="0.3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="FullRestorationNavRS_2" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="36500"/>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="bream-pike-half-saturation">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bream-vegetation-half-depletion">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical-nutrient">
      <value value="2.25"/>
      <value value="2.75"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="houseowner-types">
      <value value="&quot;homoOwners&quot;"/>
      <value value="&quot;homoOwnersEnforced&quot;"/>
      <value value="&quot;homoOwnersSocial&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="1.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-state">
      <value value="&quot;clear&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="messages?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;dynamic&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius-of-neighbors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="respond-direct?">
      <value value="true"/>
    </enumeratedValueSet>
    <steppedValueSet variable="run-number" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="target-nutrients">
      <value value="2.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="threshold-variable">
      <value value="&quot;nutrients&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tolerance-level-affectors">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;SewageDRestore&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="willingness-to-upgrade">
      <value value="0.18"/>
      <value value="0.2"/>
      <value value="0.22"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="BaselineBiggs" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="36500"/>
    <enumeratedValueSet variable="agents-uniform?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bream-pike-half-saturation">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bream-vegetation-half-depletion">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical-nutrient">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;biggs-baseline&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="houseowner-types">
      <value value="&quot;homoOwners&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="1.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-state">
      <value value="&quot;clear&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-agents?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-laststate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="messages?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;constant&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="profiling?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="radius-of-neighbors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="respond-direct?">
      <value value="true"/>
    </enumeratedValueSet>
    <steppedValueSet variable="run-number" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="threshold-variable">
      <value value="&quot;nutrients&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="target-nutrients">
      <value value="2"/>
      <value value="2.3"/>
      <value value="2.5"/>
      <value value="2.7"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tolerance-level-affectors">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;BaselineBiggs&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="willingness-to-upgrade">
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Rtipping_demoB" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="competition-bream">
      <value value="7.5E-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="competition-pike">
      <value value="2.75E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="experiment">
      <value value="&quot;transient-hysteresis&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="init-nutrients">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-bream">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-households">
      <value value="16"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-number-pike">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-agents?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-lake-per-year?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-laststate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-ticks?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-sewage-water">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mortality-pike">
      <value value="0.00225"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-series">
      <value value="&quot;constant&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nutrient-change">
      <value value="0.1"/>
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pike-expectation">
      <value value="1.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="predation-rate-pike">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="r-bream">
      <value value="0.0075"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="recycling-rate">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="regulate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="run-number">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="scenario">
      <value value="&quot;Rtipping&quot;"/>
    </enumeratedValueSet>
    <steppedValueSet variable="target-nutrients" first="0.8" step="0.4" last="2.4"/>
    <enumeratedValueSet variable="user-predation-efficiency">
      <value value="0.1"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

ruling
0.2
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 105 210
Line -7500403 true 150 150 195 210
@#$#@#$#@
0
@#$#@#$#@
